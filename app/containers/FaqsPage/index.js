import React, { Component } from 'react';
import Card from 'components/Card';
import ExpandableCard from 'containers/ExpandableCard';
import './Faq.scss';
/* eslint-disable react/prefer-stateless-function */
export class FaqsPage extends Component {
  render() {
    return (
      <div className="FAQ">
        <div className="PageTitle">
          <h1>FAQS</h1>
        </div>

        <div className="faq">
          <ExpandableCard
            cardOuter="cardButThroughProps"
            cardTitle="titleforthecard"
            cardTitleText="This is question 1"
            cardBody="cardBody class"
            cardBodyText="This is the answer to question 1"
          />
          <ExpandableCard
            cardOuter="secondcardouter"
            cardTitle="secondcardtitle"
            cardTitleText="This is question 500"
            cardBody="cardBody2 theSequel"
            cardBodyText="This is the answer to question 942"
          />
        </div>
      </div>
    );
  }
}

export default FaqsPage;

// Should be like <Card.question> => <Card.title>
// <Card.answer> => <Card.body>
