import React, { Component } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/prefer-stateless-function */
export class ExpandableCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  expandCard = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
    }));
  };
  render() {
    return (
      <div>
        <button onClick={this.expandCard}>Click here to expand</button>
        <div className={this.props.cardOuter}>
          <div className={this.props.cardTitle}>{this.props.cardTitleText}</div>
          {this.state.expanded ? (
            <div className={this.props.cardBody}>{this.props.cardBodyText}</div>
          ) : (
            ''
          )}
        </div>
      </div>
    );
  }
}

ExpandableCard.propTypes = {
  cardOuter: PropTypes.string,
  cardTitle: PropTypes.string,
  cardTitleText: PropTypes.string,
  cardBody: PropTypes.string,
  cardBodyText: PropTypes.string,
};

export default ExpandableCard;
