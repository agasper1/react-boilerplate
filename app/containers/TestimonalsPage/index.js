import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Carousel from 'containers/Carousel';
/* eslint-disable react/prefer-stateless-function */
export class TestimonalsPage extends Component {
  render() {
    const { testimonals, itemsPerPage } = this.props;
    return (
      <div className="testimonials">
        <h2>Testimonials</h2>
        <Carousel carouselItems={testimonals} itemsPerPage={itemsPerPage} />
      </div>
    );
  }
}
TestimonalsPage.propTypes = {
  testimonals: PropTypes.array,
  itemsPerPage: PropTypes.number,
};
export default TestimonalsPage;
