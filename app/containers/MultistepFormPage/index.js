import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import MultistepForm from 'containers/MultistepForm/index';
import showResults from 'containers/MultistepFormPage/showResults';
/* eslint-disable react/prefer-stateless-function */
export class MultistepFormPage extends React.PureComponent {
  render() {
    const { loading, error } = this.props;
    return (
      <article>
        <Helmet>
          <title>Multi-step form page</title>
          <meta name="description" content="Multi-step form page" />
        </Helmet>
        <div>
          <MultistepForm onSubmit={showResults} />
        </div>
      </article>
    );
  }
}

MultistepFormPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};
export default MultistepFormPage;
