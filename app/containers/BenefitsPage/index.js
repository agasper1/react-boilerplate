import React, { Component } from 'react';
import './Benefits.scss';
/* eslint-disable react/prefer-stateless-function */
export class BenefitsPage extends Component {
  render() {
    return (
      <div className="Benefits">
        <div className="Benefits-items js-benefits-items row no-gutters">
          <div className="Benefits-item col-6 col-md-4 col-lg">
            <a href="#" data-ga="Heres What You Get|Selection|High-speed data">
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-4g.png"
                  alt="4G LTE"
                />
                <h3>
                  High-speed<br />data
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  High-speed<br />data
                </h3>
                <p>
                  Your choice of 2, 5, or 10GB of 4G LTE data each month—no more
                  overpaying for data you don't use.
                </p>
              </div>
            </a>
          </div>
          <div className="Benefits-item col-6 col-md-4 col-lg">
            <a
              href="#"
              data-ga="Heres What You Get|Selection|Unlimited talk &amp; text"
            >
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-talk.png"
                  alt="talk &amp; text"
                />
                <h3>
                  Unlimited<br />talk &amp; text
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  Unlimited<br />talk &amp; text
                </h3>
                <p>Wear out your friends and loved ones all over the country</p>
              </div>
            </a>
          </div>
          <div className="Benefits-item col-6 col-md-4 col-lg">
            <a href="#" data-ga="Heres What You Get|Selection|Free sim card">
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-sim-shadow.png"
                  alt="sim card"
                />
                <h3>
                  Free<br />sim card
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  Free<br />sim card
                </h3>
                <p>
                  Some carriers charge for this. We think that's lame. Ours is
                  included free and comes in selectable standard, micro and nano
                  sizes.
                </p>
              </div>
            </a>
          </div>
          <div className="Benefits-item col-6 col-md-4 col-lg">
            <a
              href="#"
              data-ga="Heres What You Get|Selection|Nationwide coverage"
            >
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-nationwide.png"
                  alt="nationwide"
                />
                <h3>
                  Nationwide<br />coverage
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  Nationwide<br />coverage
                </h3>
                <p>
                  Yup, we're coast to coast on the nation's fastest,
                  most-advanced network (i.e. it's legit).
                </p>
              </div>
            </a>
          </div>
          <div className="Benefits-item col-6 col-md-4 col-lg">
            <a href="#" data-ga="Heres What You Get|Selection|Mobile hotspot">
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-hotspot.png"
                  alt="hotspot"
                />
                <h3>
                  Mobile<br />hotspot
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  Mobile<br />hotspot
                </h3>
                <p>
                  Is it hot in here, or is it just us? Oh, it's both because we
                  let you turn your device into a WiFi hotspot.
                </p>
              </div>
            </a>
          </div>
          <div className="Benefits-item col-6 col-md-4 d-lg-none">
            <a href="#" data-ga="Heres What You Get|Selection|Foxy mascot">
              <div className="Benefits-itemCard">
                <img
                  src="https://www.mintmobile.com/wp-content/themes/mintmobile/images/icon-fox.png"
                  alt="fox"
                />
                <h3>
                  Foxy<br />mascot
                </h3>
              </div>
              <div className="Benefits-itemDesc">
                <h3>
                  Foxy<br />mascot
                </h3>
                <p>Pretty cool, not gonna lie.</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
export default BenefitsPage;
