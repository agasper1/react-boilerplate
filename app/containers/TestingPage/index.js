import React, { Component } from 'react';
// import Carousel from 'containers/Carousel';
// import PlanCard from 'components/PlanCard';
// import BenefitsPage from 'containers/BenefitsPage';
// import ExpandableCard from 'containers/ExpandableCard';
// import FaqsPage from 'containers/FaqsPage';
import Banner from 'components/Banner';
import MintMobileLogo from 'components/Header/mintmobile';
import img from 'images/fox-balloon.png';
import TestimonalsPage from 'containers/TestimonalsPage';
import Balloon from 'containers/Balloon';
import Card from 'components/Card';
import { mapDispatchToProps } from '../SimpleForm';
/* eslint-disable react/prefer-stateless-function */
export class TestingPage extends Component {
  render() {
    const testimonals = [
      <Card image={<img src={require('images/phone.png')} height="61px" />} />,
      <Card image={<img src={require('images/bulk.png')} height="67px" />} />,
      <Card
        image={<img src={require('images/noOverages.png')} width="70px" />}
      />,
    ];

    return (
      <div className="Plans">
        <p>Jello world</p>

        <Balloon />
        <Banner
          bannerColor="#50B5BB"
          bannerClass="col-sm-12"
          bannerText="Mint Mobile works on most unlocked GSM Devices"
          svgIcon={
            <MintMobileLogo
              svgIconScale="%"
              svgIconWidth="50"
              svgIconHeight="100"
            />
          }
        />

        <div className="row">
          <TestimonalsPage testimonals={testimonals} itemsPerPage={3} />
        </div>
      </div>
    );
  }
}
export default TestingPage;
