import { call, put, select, takeLatest } from 'redux-saga/effects';
import { DO_THING } from 'containers/SimpleForm/constants';
import { thingDone, thingDoneError } from 'containers/SimpleForm/actions';
import { makeSelectFirstName } from 'containers/SimpleForm/selectors';

export function* doThing() {
  const firstName = yield select(makeSelectFirstName());

  console.log('firstName', firstName);
  try {
    yield put(thingDone(firstName));
  } catch (error) {
    console.log('error', error);
    yield put(thingDoneError(error));
  }
}

export default function* thingData() {
  yield takeLatest(DO_THING, doThing);
}
