import { fromJS } from 'immutable';
import { DO_THING, THING_DONE } from './constants';

export const initialState = fromJS({
  thing: 'abc',
  name: {
    first: '',
    last: '',
  },
  firstName: '',
  lastName: '',
});
export function simpleReducer(state = initialState, action) {
  // console.log('simpleReducer - state:', state);
  console.log('simpleReducer - action:', action);
  switch (action.type) {
    case DO_THING:
      // return state.setIn(['name', 'first'], action.payload);
      // return state.set('firstName', action.payload);
      return state;
    case '@@redux-form/CHANGE':
      console.log('action.meta', action.meta);
      console.log('action.meta.field', action.meta.field);
      return state.set(action.meta.field, action.payload);
    case THING_DONE:
    default:
      return state;
  }
}

const reducer = simpleReducer;
// combine the reducer above with the redux form reducer
// const reducer = combineReducers({
//   sampleReducer: simpleReducer,
//   form: formReducer,
// });
// console.log('reducer simple', reducer);
// console.log('hello');

export default reducer;
