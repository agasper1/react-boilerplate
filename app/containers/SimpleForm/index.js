import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form/immutable';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from './reducer';
import { doThing } from './actions';
import saga from './saga';

// import { makeSelectFirstName } from './selectors';

/* eslint-disable react/prefer-stateless-function */
export class SelectingFormValuesForm extends Component {
  render() {
    const {
      favoriteColorValue,
      fullName,
      handleSubmit,
      hasEmailValue,
      pristine,
      reset,
      submitting,
    } = this.props;
    return (
      <form onSubmit={this.props.onSubmitForm}>
        <div>
          <label>First Name</label>
          <div>
            <Field
              name="firstName"
              component="input"
              type="text"
              placeholder="First Name"
            />
          </div>
        </div>
        <div>
          <label>Last Name</label>
          <div>
            <Field
              name="lastName"
              component="input"
              type="text"
              placeholder="Last Name"
            />
          </div>
        </div>
        <div>
          <label htmlFor="hasEmail">Has Email?</label>
          <div>
            <Field
              name="hasEmail"
              id="hasEmail"
              component="input"
              type="checkbox"
            />
          </div>
        </div>
        {hasEmailValue && (
          <div>
            <label>Email</label>
            <div>
              <Field
                name="email"
                component="input"
                type="email"
                placeholder="Email"
              />
            </div>
          </div>
        )}
        <div>
          <label>Favorite Color</label>
          <div>
            <Field name="favoriteColor" component="select">
              <option />
              <option value="#ff0000">Red</option>
              <option value="#00ff00">Green</option>
              <option value="#0000ff">Blue</option>
            </Field>
          </div>
        </div>
        {favoriteColorValue && (
          <div
            style={{
              height: 80,
              width: 200,
              margin: '10px auto',
              backgroundColor: favoriteColorValue,
            }}
          />
        )}
        <div>
          <button type="submit" disabled={pristine || submitting}>
            Submit {fullName}
          </button>
          <button
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button>
        </div>
      </form>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: event => {
      if (event !== undefined && event.preventDefault) event.preventDefault();
      dispatch(doThing());
    },
  };
}

const selector = formValueSelector('selectingFormValues');
const mapStateToProps = state => {
  const { firstName, lastName } = selector(state, 'firstName', 'lastName');
  return {
    firstName,
    lastName,
  };
};

// The order of the decoration does not matter.

// Decorate with redux-form
const withReduxForm = reduxForm({
  form: 'selectingFormValues', // a unique identifier for this form
});

const withReducer = injectReducer({ key: 'simpleForm', reducer });
const withSaga = injectSaga({ key: 'simpleSaga', saga });

// Decorate with connect to read form values
// Every function gets state made available to it when passed into connect
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReduxForm,
  withReducer,
  withSaga,
  withConnect,
)(SelectingFormValuesForm);
