import { DO_THING, THING_DONE, DO_THING_ERROR } from './constants';

export function doThing() {
  return {
    type: DO_THING,
  };
}
export function thingDone(stuff) {
  return {
    type: THING_DONE,
    data: stuff,
  };
}

export function thingDoneError(error) {
  return {
    type: DO_THING_ERROR,
    error,
  };
}
