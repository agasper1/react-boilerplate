import { createSelector } from 'reselect';

// get('keyNameUsedInInjectReducer');
const selectFormState = state => state.get('simpleForm');
// const selectHome = state => state.get('home', initialState);
// createSelector(selectHome, homeState => homeState.get('username'));
// createSelector(state, someValue)
const makeSelectFirstName = () =>
  createSelector(
    selectFormState,
    // firstNameState => firstNameState.get('thing'),
    formSelector => formSelector.get('firstName'),
  );

export { makeSelectFirstName };
