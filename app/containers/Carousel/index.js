import React, { Component } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/prefer-stateless-function */
export class Carousel extends Component {
  constructor(props) {
    super(props);
    console.log('constructor props', props);
    this.state = {
      page: 0,
      visibleCarouselItems: props.carouselItems.slice(0, props.itemsPerPage),
      totalNumberOfPages: this.calculateTotalPages(
        props.carouselItems.length,
        props.itemsPerPage,
      ),
      pages: this.createPages(this.totalNumberOfPages),
      // debugging: this.debuggingFunction(this),
    };
  }

  componentDidMount() {
    this.setState({ pages: this.createPages(this.state.totalNumberOfPages) });
  }

  debuggingFunction(thingToConsoleLog) {
    console.log('debuggingFunction:', thingToConsoleLog);
  }

  createPages(totalPages = 0) {
    const pages = [];
    for (let i = 0; i < totalPages; i += 1) {
      // Arrays may start at 0, but pages start at 1...
      pages.push(i + 1);
    }
    return pages;
  }
  calculateTotalPages(totalItems, itemsPerPage) {
    return Math.ceil(totalItems / itemsPerPage);
  }
  // I really should have done it like {1: [<div />, <div />, <div />] , 2: [<div />, <div />, <div />]}
  getVisibleCarouselItems = () => this.state.visibleCarouselItems;
  getTotalNumberOfPages = () => this.state.totalNumberOfPages;
  getPages = () => this.state.pages;
  getCurrentPage = () => this.state.page;
  goToPage = page => {
    let nextPage = page;
    const upperLimit = this.props.itemsPerPage * nextPage;
    const lowerLimit = upperLimit - this.props.itemsPerPage;
    this.setState(visibleCarouselItems => ({
      visibleCarouselItems: this.props.carouselItems.slice(
        lowerLimit,
        upperLimit,
      ),
    }));
    nextPage -= 1;
    console.log('goToPage - nextPage:', nextPage);
    this.setState(({ page }) => ({ page: nextPage }));
  };

  nextPage = () => {
    // this should really be a reducer/this gets at what a reducer does
    let nextPage = this.state.page + 1; // I'm not feeling clever enough to use state correctly. So queue the
    // Go back to the beginning if the next page would be greater than the total length

    if (nextPage >= this.state.totalNumberOfPages) {
      nextPage = 0;
    } else {
      nextPage = nextPage; // redundant assignment but just want to be clear what's happening;
    }
    this.setState(({ page }) => ({ page: nextPage }));
    const offset = this.props.itemsPerPage * nextPage;
    this.setState(visibleCarouselItems => ({
      visibleCarouselItems: this.props.carouselItems.slice(
        offset,
        offset + this.props.itemsPerPage,
      ),
    }));
  };
  previousPage = () => {
    let nextPage = this.state.page ? this.state.page - 1 : this.state.page;
    // You're all the way at the start, jump to the end.
    if (!nextPage) {
      nextPage = this.state.totalNumberOfPages;
    } else {
      nextPage = nextPage;
    }
    // console.log('currentPage: this.state.page', this.state.page);
    // console.log('nextPage:', nextPage);
    this.setState(({ page }) => ({ page: nextPage }));
    console.log('nextPage', nextPage);

    const upperLimit = this.props.itemsPerPage * nextPage;
    const lowerLimit = upperLimit - this.props.itemsPerPage;
    console.log('lowerLimit', lowerLimit);
    console.log('upperLimit', upperLimit);
    this.setState(visibleCarouselItems => ({
      visibleCarouselItems: this.props.carouselItems.slice(
        lowerLimit,
        upperLimit,
      ),
    }));
  };
  render() {
    const visibleCarouselItems =
      this.getVisibleCarouselItems() ||
      this.props.carouselItems(0, this.props.itemsPerPage);
    const pages = this.getPages() || [];
    // const pages = [1, 2, 3];
    return (
      <div className="carousel">
        {visibleCarouselItems.map(item => (
          <div className="carouselItem">{item}</div>
        ))}
        {pages.map(pageIndicator => (
          <button
            key={pageIndicator}
            onClick={() => this.goToPage(pageIndicator)}
          >
            {pageIndicator}
          </button>
        ))}

        <button onClick={this.nextPage}>Next page</button>
        <button onClick={this.previousPage}>Previous page</button>
      </div>
    );
  }
}

Carousel.propTypes = {
  carouselItems: PropTypes.array,
  itemsPerPage: PropTypes.number,
};

export default Carousel;

// slice = return the selected elements as a new array object. So don't mutate with splice pls
