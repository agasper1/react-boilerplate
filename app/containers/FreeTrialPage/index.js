import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import Card from 'components/Card';
import PlanCard from 'components/PlanCard';
import Banner from 'components/Banner';
import CountdownTimer from 'containers/CountdownTimer';
import CoveragePage from 'containers/CoveragePage';
import TestimonalsPage from 'containers/TestimonalsPage';
import BenefitsPage from 'containers/BenefitsPage';
import FaqsPage from 'containers/FaqsPage';
import messages from './messages';
import MintMobileLogo from 'components/Header/mintmobile';

/* eslint-disable react/prefer-stateless-function */
export class FreeTrialPage extends Component {
  render() {
    const testimonals = [
      <Card title="Loved1" body="it" />,
      <Card title="Loved2" body="it" />,
      <Card title="Loved3" body="it" />,
      <Card title="Loved4" body="it" />,
      <Card title="Loved5" body="it" />,
      <Card title="Loved6" body="it" />,
      <Card title="Loved7" body="it" />,
      <Card title="Loved8" body="it" />,
      <Card title="Loved9" body="it" />,
    ];

    return (
      <article>
        <Helmet>
          <title>Free trial page</title>
          <meta name="description" content="Free trial offer" />
        </Helmet>
        <div className="container-fluid">
          <div className="row">
            <FormattedMessage {...messages.freeTrialMessage} />
          </div>

          <div className="row">
            <CountdownTimer />
          </div>
          <div className="row">
            <CoveragePage />
          </div>
          <div className="deviceCompatiability">
            <p>Mint Mobile works on most unlocked GSM Devices</p>
          </div>
          <div className="row">
            <Banner
              bannerColor="#8E8E8E"
              bannerClass="col"
              bannerTextAsObject={
                <div className="Banner">
                  <ul>
                    TRIAL INCLUDES:
                    <li>
                      <span className="bold">100MB</span> 4G LTE DATA
                    </li>
                    <li>
                      <span className="bold">100</span> MINS
                    </li>
                    <li>
                      <span className="bold">100</span> TEXT
                    </li>
                    <li>
                      <span className="bold">3-IN-1</span> SIM CARD
                    </li>
                  </ul>
                </div>
              }
            />
          </div>
          <img src="" alt="sim pictures go here" />
          <div className="row">
            <h2>Loved your trial?</h2>
            <h2>GET YOUR $5 BACK ANYWAY!</h2>
            <p>
              After your RISK-FREE trial is up, you can still get your $5 back
              when you upgrade to any full Mint Mobile plan! Just check out the
              cray-cray pricing on our 3-month plans -- how are you gonna say no
              to that?
            </p>
          </div>
          <div className="Plans">
            <PlanCard />
            <PlanCard />
            <PlanCard />
          </div>

          <div className="row">
            <p>Full plans include</p>
            <BenefitsPage />
          </div>
          <div className="row">
            <TestimonalsPage testimonals={testimonals} itemsPerPage={3} />
          </div>
          <div className="row">
            <FaqsPage />
          </div>
        </div>
      </article>
    );
  }
}

FreeTrialPage.propTypes = {};

export default FreeTrialPage;
