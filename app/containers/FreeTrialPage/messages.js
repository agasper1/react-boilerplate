/*
 * FreeTrialPage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  freeTrialMessage: {
    id: 'boilerplate.containers.FreeTrialPage.free_trial.header',
    defaultMessage: 'Get your trial now',
  },
  startProjectMessage: {
    id: 'boilerplate.containers.FreeTrialPage.free_trial.message',
    defaultMessage: '(Easy tiger - limit one per household)',
  },
});
