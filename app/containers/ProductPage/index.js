import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import ProductList from 'components/ProductList';
import List from 'components/List';
import Card from 'components/Card';
import messages from './messages';
import SampleColumn from 'components/SampleGrid';

/* eslint-disable react/prefer-stateless-function */
export default class ProductPage extends React.Component {
  render() {
    const products = [
      {
        title: 'Product A',
        body: 'This is some text for product A',
      },
      { title: 'Product B', body: 'This is some text for product B' },
    ];
    return (
      <div>
        <Helmet>
          <title>Products page</title>
          <meta name="description" content="Product page" />
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <ProductList products={products} />
        <SampleColumn />
      </div>
    );
  }
}

ProductPage.propTypes = {};
