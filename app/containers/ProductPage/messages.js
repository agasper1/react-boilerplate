/*
* ProductPage Messages
* This contains all the text for the ProductPage component 
*/

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'boilerplate.containers.ProductPage.header',
    defaultMessage: 'Products',
  },
});
