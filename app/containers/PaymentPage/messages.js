/**
 * PaymentPage messages
 *
 * This contains all the text for the Payment Page component
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'boilerplaye.containers.PaymentPage.header',
    defaultMessage: 'Payment Page',
  },
});
