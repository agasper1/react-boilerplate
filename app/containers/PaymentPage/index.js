import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import H1 from 'components/H1';
import PaymentForm from 'containers/PaymentForm/index';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class PaymentPage extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Payment Page</title>
          <meta name="description" content="Just the payment page" />
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <PaymentForm />
      </div>
    );
  }
}

export default PaymentPage;
