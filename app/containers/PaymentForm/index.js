import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { generatePaymentToken } from './actions';
import saga from './saga';
import reducer from './reducer';
import { makeSelectFormValues as selector } from './selectors';

/* eslint-disable react/prefer-stateless-function */
export class PaymentForm extends Component {
  render() {
    const { pristine, submitting, reset } = this.props;
    return (
      <form onSubmit={this.props.onSubmitForm}>
        <div>
          <label htmlFor="creditCardNumber">Credit card number</label>
          <Field
            name="creditCardNumber"
            component="input"
            type="text"
            placeholder="Credit card number"
          />
        </div>
        <div>
          <label htmlFor="cvcNumber"> CVC/CV2 Number</label>
          <Field
            name="cvcNumber"
            component="input"
            type="text"
            placeholder="CVC/CV2 Number"
          />
        </div>
        <div>
          <label htmlFor="expirationMonth">Exp. month</label>
          <Field
            name="expirationMonth"
            component="input"
            type="text"
            placeholder="Expiration Month"
          />
          <label htmlFor="expirationYear">Exp. year</label>
          <Field
            name="expirationYear"
            component="input"
            type="text"
            placeholder="Expiration Year"
          />
        </div>
      </form>
    );
  }
}

PaymentForm.propTypes = {
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  reset: PropTypes.func,
  onSubmitForm: PropTypes.func,
};

const withReduxForm = reduxForm({
  form: 'PaymentForm',
  destroyOnUnmount: false,
});

const withReducer = injectReducer({ key: 'payment', reducer });
const withSaga = injectSaga({ key: 'payment', saga });

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: event => {
      if (event !== undefined && event.preventDefault) event.preventDefault();
      dispatch(generatePaymentToken());
    },
  };
}

const mapStateToProps = state => {
  const ccNumber = selector(state, 'creditCardNumber');
  const cvcNumber = selector(state, 'cvcNumber');
  const expYear = selector(state, 'expirationYear');
  const expMonth = selector(state, 'expirationMonth');
  return {
    ccNumber,
    cvcNumber,
    expYear,
    expMonth,
  };
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReduxForm,
  withReducer,
  withSaga,
  withConnect,
)(PaymentForm);
