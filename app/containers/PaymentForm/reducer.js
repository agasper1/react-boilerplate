import { fromJS } from 'immutable';
import {
  GENERATE_PAYMENT_TOKEN,
  GENERATE_PAYMENT_TOKEN_SUCCESS,
  GENERATE_PAYMENT_TOKEN_ERROR,
} from './constants';
export const initialState = fromJS({
  creditCardNumber: '',
  cvcNumber: '',
  expirationDate: {
    month: '',
    year: '',
  },
  loading: false,
  error: false,
  paymentToken: '',
});
// The payment token probably doesn't belong to the form...

function paymentReducer(state = initialState, action) {
  switch (action.type) {
    case GENERATE_PAYMENT_TOKEN:
      return state.set('loading', true).set('error', false);
    case GENERATE_PAYMENT_TOKEN_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('paymentToken', action.paymentToken);
    case GENERATE_PAYMENT_TOKEN_ERROR:
      return state.set('loading', false).set('error', true);
    default:
      return state;
  }
}
const reducer = paymentReducer;
export default reducer;
