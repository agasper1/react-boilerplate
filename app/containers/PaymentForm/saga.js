import { call, put, select, takeLatest } from 'redux-saga/effects';
import { GENERATE_PAYMENT_TOKEN } from 'containers/PaymentForm/constants';

import {
  generatePaymentToken,
  paymentTokenGeneratedError,
} from 'containers/PaymentForm/actions';

import request from 'utils/request';
import { makeSelectFormValues } from 'containers/PaymentForm/selectors';

/**
 * Get payment token from Vantiv
 */
export function* getPaymentToken() {
  let formValues = yield select(makeSelectFormValues());
  formValues = formValues.get('values');
  const ccNumber = formValues.get('creditCardNumber');
  const cvcNumber = formValues.get('cvcNumber');
  const expirationMonth = formValues.get('expirationMonth');
  const expirationYear = formValues.get('expirationYear');
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* paymentData() {
  yield takeLatest(GENERATE_PAYMENT_TOKEN, getPaymentToken);
}
