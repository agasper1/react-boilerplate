import {
  GENERATE_PAYMENT_TOKEN,
  GENERATE_PAYMENT_TOKEN_SUCCESS,
  GENERATE_PAYMENT_TOKEN_ERROR,
} from './constants';
export function generatePaymentToken() {
  return {
    type: GENERATE_PAYMENT_TOKEN,
  };
}

export function paymentTokenGenerated(paymentToken) {
  return {
    type: GENERATE_PAYMENT_TOKEN_SUCCESS,
    paymentToken,
  };
}

export function paymentTokenGeneratedError(error) {
  return {
    type: GENERATE_PAYMENT_TOKEN_ERROR,
    error,
  };
}
