import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import initialState from './reducer';

function getFormState(state, form) {
  return state.get('form').get(form);
}

function selectFormState(state, form) {
  return state.get('form').get(form);
}

const makeSelectFormValues = () => {
  createSelector(selectFormState, state => getFormState(state, 'PaymentForm'));
};

export { makeSelectFormValues };
