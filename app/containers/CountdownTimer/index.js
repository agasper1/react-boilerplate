import React, { Component } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/prefer-stateless-function */
export class CountdownTimer extends Component {
  constructor(props) {
    super(props);

    const second = this.secondsUntilMidnight();
    this.state = {
      hoursRemaining: Math.floor(second / 60 / 60),
      minutesRemaining: Math.floor((second / 60) % 60),
      secondsRemaining: Math.floor(second % 60),
    };
  }

  secondsUntilMidnight() {
    const targetDate = new Date();
    targetDate.setHours(24);
    targetDate.setMinutes(0);
    targetDate.setSeconds(0);

    return (targetDate.getTime() - new Date().getTime()) / 1000;
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    const second = this.secondsUntilMidnight();

    this.setState({
      hoursRemaining: Math.floor(second / 60 / 60),
      minutesRemaining: Math.floor((second / 60) % 60),
      secondsRemaining: Math.floor(second % 60),
    });
  }

  render() {
    return (
      <div>
        <h2>Offer Expires In:</h2>
        <h3>
          {this.state.hoursRemaining} HRS : {this.state.minutesRemaining} MINS :{' '}
          {this.state.secondsRemaining} SECS
        </h3>
      </div>
    );
  }
}

CountdownTimer.propTypes = {};

export default CountdownTimer;
