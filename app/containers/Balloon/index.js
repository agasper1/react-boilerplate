import React, { Component } from 'react';
import Img from 'components/Img';
import FoxBalloon from 'images/fox-balloon.png';
import './Balloon.scss';
/* eslint-disable react/prefer-stateless-function */
export class Ballon extends Component {
  componentDidMount() {
    console.log('component mounted');
  }
  render() {
    return (
      <div className="Balloon">
        <Img src={FoxBalloon} width="172px" />
      </div>
    );
  }
}

export default Ballon;

// Really uninspired on my part.
// This should def be closer to the Progress/NavBar style of definition
