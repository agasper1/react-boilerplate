# Balloon component

Review this article: https://medium.com/@bjorn.holdt/react-animations-101-css-transitions-9c1050c2bc9d
Very rough sketch of where this should go

Here is an example of some DOM event being used to change classNames related to animation key-frames: https://codepen.io/bjornholdt/pen/zZyEga

Navbar: https://codepen.io/bjornholdt/pen/MpXmmL
Start here.

Can you get a div to change color after you click it?
Can you get a div to move forward and back like in the progress bar example?
Can you get a div to move around related to the scroll on the page?

v1 sketch

const BA
