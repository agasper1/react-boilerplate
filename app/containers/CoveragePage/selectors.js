const getHasCoverage = state => state.get('coverage');
const getGlobalState = state => state.get();
export { getHasCoverage, getGlobalState };
