import { select, takeEvery, put } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import { coverageCheckedQualityError } from '../CoverageForm/actions';

import { CHECK_COVERAGE_SUCCESS } from '../CoverageForm/constants';

import { getHasCoverage } from './selectors';
export function* redirectToSite() {
  try {
    const coverageState = yield select(getHasCoverage);
    const hasCoverageState = coverageState.get('hasCoverage', false);

    if (hasCoverageState) {
      // Crude redirect

      const newUrl = `${process.env.WEB_URL}${process.env.PRODUCT_ID}`;
      document.location = newUrl;
    } else {
      // THROW FORM error
      throw new SubmissionError({
        zipcode: "We don't fox with that zipcode.",
      });
    }
  } catch (error) {
    yield put(coverageCheckedQualityError(error));
  }
}

export default function* watchForCoverage() {
  yield takeEvery(CHECK_COVERAGE_SUCCESS, redirectToSite);
}
