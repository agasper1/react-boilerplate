/**
 * CoveragePage
 *
 * Trying to debug the coverage form
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import Banner from 'components/Banner';
import MintMobileLogo from 'components/Header/mintmobile';

import injectSaga from 'utils/injectSaga';
import { connect } from 'react-redux';
import { compose } from 'redux';

import CoverageForm from 'containers/CoverageForm/index';
import saga from './saga';

import './CoveragePage.scss';

/* eslint-disable react/prefer-stateless-function */
export class CoveragePage extends Component {
  render() {
    const { loading, error } = this.props;
    return (
      <article>
        <Helmet>
          <title>Coverage Page</title>
          <meta name="description" content="Just the coverage form" />
        </Helmet>
        <div className="Coverage">
          <div className="Coverage-title">
            <h3>Get your</h3>
            <h3>Trial Now</h3>
          </div>
          <div className="Coverage-body">
            <CoverageForm />
          </div>
          <span className="legalText">
            <p>
              By continuing, you agree to our terms of use and privacy policy
            </p>
          </span>
        </div>
        <Banner
          bannerColor="#50B5BB"
          bannerClass="col-sm-12"
          bannerText="Mint Mobile works on most unlocked GSM Devices"
          svgIcon={
            <MintMobileLogo
              svgIconScale="%"
              svgIconWidth="50"
              svgIconHeight="100"
            />
          }
        />
      </article>
    );
  }
}

const withSaga = injectSaga({ key: 'coveragePage', saga });

CoveragePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

// export default CoveragePage;
export default compose(withSaga)(CoveragePage);
