/**
 * Makes call to coverage api
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { CHECK_COVERAGE } from 'containers/CoverageForm/constants';
import axios from 'axios';
import {
  coverageChecked,
  coverageCheckedError,
} from 'containers/CoverageForm/actions';

import request from 'utils/request';
import { makeSelectFormValues } from 'containers/CoverageForm/selectors';

function apiCall(url) {
  return axios.get(url);
}
/**
 * Coverage api request/response handler
 */

export function* getCoverage() {
  // const formValues = yield select(makeSelectCoverageFormValues());
  let formValues = yield select(makeSelectFormValues());
  formValues = formValues.get('values');
  const zipcode = formValues.get('zipcode');
  const emailAddress = formValues.get('emailAddress');
  const requestURL = `${process.env.COVERAGE_API}${zipcode}`;

  try {
    // const coverageResponse = yield call(request, requestURL);

    const coverageResponse = yield call(apiCall, requestURL);
    console.log('getCoverage - coverageResponse:', coverageResponse);
    yield put(coverageChecked(coverageResponse, zipcode));
  } catch (error) {
    yield put(coverageCheckedError(error));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* coverageData() {
  // Watches for CHECK_COVERAGE actions and calls getCoverage when one comes in.
  // takeLatest - Only the result of the latest API call is applied
  // Cancelled automatically on component unmount
  yield takeLatest(CHECK_COVERAGE, getCoverage);
}
