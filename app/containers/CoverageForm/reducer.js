/**
 * CoverageReuder
 *
 * Not entirely sure if this is relevant
 *
 *
 */

import { fromJS } from 'immutable';

import {
  CHECK_COVERAGE,
  CHECK_COVERAGE_SUCCESS,
  CHECK_COVERAGE_ERROR,
  CHECK_COVERAGE_QUALITY_ERROR,
} from './constants';
import { MINIMUM_QUALITY_VALUE } from './coverage_constants';

// The initial state should have no zip code
export const initialState = fromJS({
  zipcode: '',
  coverageInformation: false,
  error: false,
  loading: false,
  emailAddress: '',
  hasCoverage: false,
});

/**
 * @name isSufficientQuality
 * @param {object} coverageInformation
 * @param {object | number } quality - Numeric value that the coverage API should return and put on the object
 * @description Do the logic for whether or not to set `hasCoverage` to true/false
 * Pessimistic that they wont
 */
function isSufficientQuality(coverageInformation) {
  const { quality } = coverageInformation.data;
  if (parseInt(quality, 10) >= MINIMUM_QUALITY_VALUE) {
    return true;
  }
  return false;
}

function coverageReducer(state = initialState, action) {
  // console.log('action.payload', action.type);
  // console.log('coverageReducer state', state);
  switch (action.type) {
    case CHECK_COVERAGE:
      // Remove non-numeric characters
      return state
        .set('loading', true)
        .set('error', false)
        .set('coverageInformation', false);
    case CHECK_COVERAGE_SUCCESS:
      return state
        .set('coverageInformation', action.coverageInformation)
        .set('loading', false)
        .set('hasCoverage', isSufficientQuality(action.coverageInformation));
    case CHECK_COVERAGE_ERROR:
      return state.set('error', action.error).set('loading', false);
    case CHECK_COVERAGE_QUALITY_ERROR:
      return state.set('error', action.error);
    default:
      return state;
  }
}

const reducer = coverageReducer;

export default reducer;
