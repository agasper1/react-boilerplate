const validate = values => {
  const errors = { zipcode: '' };
  // Zipcode regex https://stackoverflow.com/questions/2577236/regex-for-zip-code
  // zipcode - 92603 => 5 digits

  // Someone please figure out how to have multiple errors. I have no idea what I did before, but at one point, I was able to have multiple error messgaes...
  // which makes this even more troubling
  if (!values.zipcode) {
    errors.zipcode += 'Required';
  } else if (/[a-zA-z]+/i.test(values.zipcode)) {
    const numbersOnlyMessage = 'Only numeric characters allowed';
    errors.zipcode += numbersOnlyMessage;
  }
  // This should be a map or something rather than relying on me to remember to add a space
  if (values.zipcode) {
    // I don't want to show the error messgae until on blur
    if (values.zipcode.length > 5) {
      const validZipCodeLengthMessage = 'Please enter a 5 digit US zip code';
      errors.zipcode += validZipCodeLengthMessage;
    }
  }
  if (!values.emailAddress) {
    errors.email = 'Required';
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailAddress)
  ) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

export default validate;
