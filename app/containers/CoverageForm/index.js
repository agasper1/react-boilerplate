import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { compose } from 'redux';

import renderField from 'utils/renderField';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import validate from './validate';
import { checkCoverage } from './actions';
import saga from './saga';
import reducer from './reducer';
import { makeSelectFormValues as selector } from './selectors';

// Touched && dirty => less aggressive form validation

/* eslint-disable react/prefer-stateless-function */
export class CoverageForm extends Component {
  render() {
    const { pristine, submitting, invalid } = this.props;

    return (
      <form onSubmit={this.props.onSubmitForm}>
        <div className="coverageErrors">
          {this.props.coverageError.errors &&
          this.props.coverageError.errors.zipcode
            ? this.props.coverageError.errors.zipcode
            : ''}
        </div>
        <div>
          <Field
            name="zipcode"
            type="text"
            component={renderField}
            label="Zip code"
          />
        </div>
        <div>
          <Field
            name="emailAddress"
            type="email"
            component={renderField}
            label="Email"
          />
        </div>
        <div>
          <button
            className="submit"
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            <p>Gimmie my free trial</p>
          </button>
        </div>
        <div>{console.log(this.props.coverageError)}</div>
      </form>
    );
  }
}

CoverageForm.propTypes = {
  pristine: PropTypes.bool,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
  onSubmitForm: PropTypes.func,
  coverageError: PropTypes.object || PropTypes.bool,
};

// Action stuff
export function mapDispatchToProps(dispatch) {
  return {
    // onChangeZipCode: event => dispatch(changeZipCode(event.target.value)),
    onSubmitForm: event => {
      if (event !== undefined && event.preventDefault) event.preventDefault();
      // Prevent the default form behavior and dispatch our saga
      // console.log('hello world');
      dispatch(checkCoverage());
    },
  };
}

// mapStateToProps seems to get called again
const mapStateToProps = state => {
  const zipcode = selector(state, 'zipcode');
  const emailAddress = selector(state, 'emailAddress');
  const coverageError = state.get('coverage').get('error');
  return {
    zipcode,
    emailAddress,
    coverageError,
  };
};

const withReduxForm = reduxForm({
  form: 'CoverageForm',
  validate,
  destroyOnUnmount: true,
});

// The createReducer function accepts additional reducers being injected
// `reducerKey: reducerFunctionName` => ...injectedReducer
// So lets inject the coverage reducer with the key of and a function/callback of whatever `reducer` is
const withReducer = injectReducer({ key: 'coverage', reducer });
// Same for the sagas
const withSaga = injectSaga({ key: 'coverage', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReduxForm,
  withReducer,
  withSaga,
  withConnect,
)(CoverageForm);
