import {
  CHANGE_ZIPCODE,
  CHECK_COVERAGE,
  CHECK_COVERAGE_SUCCESS,
  CHECK_COVERAGE_ERROR,
  CHECK_COVERAGE_QUALITY_ERROR,
} from './constants';

/**
 * Manipulate the form state - I couldn't figure out redux form + selectors and I'm sick of it
 *
 * @return {object} An object with type of CHANGE_ZIPCODE
 */

export function changeZipCode(zipcode) {
  // console.log('changeZipCode, zipcode:', zipcode);
  return {
    type: CHANGE_ZIPCODE,
    zipcode,
  };
}

/**
 * Check the coverage, this action starts the request saga
 *
 * @return {object} An action object with type of CHECK_COVERAGE ß
 */
export function checkCoverage() {
  return {
    type: CHECK_COVERAGE,
  };
}

/**
 * Dispatched when the coverage response is loaded by the request saga
 * @param {object | JSON } coverageInformation - The coverage information
 * @param {string} zipcode The current zipcode
 *
 * @return {object} An object with a type of CHECK_COVERAGE_SUCCESS passing the response
 */

export function coverageChecked(coverageInformation, zipcode) {
  return {
    type: CHECK_COVERAGE_SUCCESS,
    coverageInformation,
    zipcode,
  };
}

/**
 * Dispatched when the coverage check fails
 *
 * @param {object} The error message
 *
 * @return {object} An action object with a type of CHECK_COVERAGE_ERROR passing the error
 */

export function coverageCheckedError(error) {
  return {
    type: CHECK_COVERAGE_ERROR,
    error,
  };
}

export function coverageCheckedQualityError(error) {
  return {
    type: CHECK_COVERAGE_QUALITY_ERROR,
    error,
  };
}
