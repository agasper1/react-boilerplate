/**
 * CoverageForm selectors
 *
 */

import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { initialState } from './reducer';

// import { formValueSelector } from 'redux-immutable';
// import { getIn } from 'immutable';
// Getting a field value -> create selector with scope
// Use selector -> selector(state, 'stateKeyWithValueYouWant');
// const coverageFormSelector = formValueSelector('CoverageForm'); // Use the form name to start the selection
const selectZipCode = state => state.get('coverage', initialState);
const selectEmailAddress = state => state.get('coverage', initialState);
// Get the top level form state slice
const selectFormState = state =>
  fromJS({
    form: state.get('form', initialState),
  });
// state => state.get('form', initalState) is actually what getFormState defaults to

function getFormState(state, form) {
  return state.get('form').get(form);
}

// Create the selector
const selectHasCoverage = state => state.get('hasCoverage', initialState);

// injectedSelectors
const makeSelectFormValues = () =>
  createSelector(selectFormState, state => getFormState(state, `CoverageForm`));

// This seems to be analogous to formSelector(state, 'fieldName');
const makeSelectZipCode = () =>
  createSelector(selectZipCode, zipcodeState => zipcodeState.get('zipcode'));

const makeSelectEmailAddress = () =>
  createSelector(selectEmailAddress, emailAddressState =>
    emailAddressState.get('emailAddress'),
  );

const selectCoverageInformation = state => state.get('coverage', initialState);
const makeSelectCoverageInformation = () =>
  createSelector(selectCoverageInformation, coverageInformation =>
    coverageInformation.get('coverageInformation'),
  );

/** @name makeSelectCoverageFormValues
 * @description - Return an object with keys that are immutable maps
 */
const makeSelectCoverageFormValues = () =>
  createSelector(
    selectZipCode,
    selectEmailAddress,
    (zipcode, emailAddress) => ({
      zipcode,
      emailAddress,
    }),
  );

export {
  makeSelectFormValues,
  makeSelectZipCode,
  makeSelectEmailAddress,
  makeSelectCoverageFormValues,
  makeSelectCoverageInformation,
};
