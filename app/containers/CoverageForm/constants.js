export const CHECK_COVERAGE = 'boilerplate/CoverageForm/CHECK_COVERAGE';
export const CHECK_COVERAGE_SUCCESS =
  'boilerplate/CoverageForm/CHECK_COVERAGE_SUCCESS';
export const CHECK_COVERAGE_ERROR =
  'boilerplate/CoverageForm/CHECK_COVERAGE_ERROR';
export const CHANGE_ZIPCODE = 'boilerplate/CoverageForm/CHANGE_ZIPCODE';

export const CHECK_COVERAGE_QUALITY_ERROR =
  'boilerplate/CoverageForm/CHECK_COVERAGE_QUALITY_ERROR';
