/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import FreeTrialPage from 'containers/FreeTrialPage';
import ProductsPage from 'containers/ProductPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import CarouselPage from 'containers/CarouselPage/Loadable';
import TestingPage from 'containers/TestingPage/Loadable';

import './app.scss';

const AppWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - Mint Mobile"
        defaultTitle="Mint Mobile Free Trial Kit"
      >
        <meta name="description" content="A Mint Mobile Application" />
      </Helmet>
      <Header />
      <Switch>
        <Route exact path="/" component={FreeTrialPage} />
        <Route path="/products" component={ProductsPage} />
        <Route path="/carousel" component={CarouselPage} />
        <Route path="/testing" component={TestingPage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
      <Footer />
    </AppWrapper>
  );
}
