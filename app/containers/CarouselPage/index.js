/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import H2 from 'components/H2';
import Card from 'components/Card';
import { Carousel } from 'containers/Carousel';

/* eslint-disable react/prefer-stateless-function */
export class CarouselPage extends React.PureComponent {
  render() {
    const { loading, error } = this.props;
    const items = [
      <Card title="Hello1" body="World" />,
      <Card title="Hello2" body="World" />,
      <Card title="Hello3" body="World" />,
      <Card title="Hello4" body="World" />,
      <Card title="Hello5" body="World" />,
      <Card title="Hello6" body="World" />,
      <Card title="Hello7" body="World" />,
      <Card title="Hello8" body="World" />,
      <Card title="Hello9" body="World" />,
    ];
    return (
      <article>
        <Helmet>
          <title>Carousel Page</title>
          <meta name="description" content="Carousel page debugging" />
        </Helmet>
        <div>
          <H2>Carousel</H2>
          <Carousel carouselItems={items} itemsPerPage={3} />
        </div>
      </article>
    );
  }
}

CarouselPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

export default CarouselPage;
