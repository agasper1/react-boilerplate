/**
 * OrderPage Messages
 *
 * This contains all the text for the OrderPage component
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'boilerplate.containers.ShippingPage.header',
    defaultMessage: 'Shipping page',
  },
});
