import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import ShippingForm from 'containers/ShippingForm/index';
import messages from './messages';

// I don't think this really needs to be a class

/* eslint-disable react/prefer-stateless-function */
class ShippingPage extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>ShippingPage</title>
          <meta name="Shipping Page" content="Shipping page of application" />
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <ShippingForm />
      </div>
    );
  }
}

export default ShippingPage;
