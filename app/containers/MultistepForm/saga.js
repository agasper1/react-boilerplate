import {
  call,
  put,
  select,
  takeLatest,
  fork,
  all,
  takeEvery,
} from 'redux-saga/effects';
import { nextPage, previousPage } from 'containers/MultistepForm/actions';
import { CHECK_COVERAGE_SUCCESS } from '../CoverageForm/constants';

export function* goForward() {
  try {
    console.log('goForward');
    yield put(nextPage());
  } catch (error) {
    yield put(nextPage());
  }
}

function* logger(action) {
  const state = yield select();
  console.log('action', action);
  console.log('state after', state);
}

// Listen for CHECK_COVERAGE_SUCCESS then trigger next page
// export default function* multiStepFormSaga() {
//   yield takeEvery(CHECK_COVERAGE_SUCCESS, nextPage);
// }

export default function* watchAndLog() {
  // Watch for the CHECK_COVERAGE_SUCCESS action
  yield takeEvery(CHECK_COVERAGE_SUCCESS, goForward);
}
