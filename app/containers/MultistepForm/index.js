import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import CoverageForm from 'containers/CoverageForm/index';
import ShippingForm from 'containers/ShippingForm/index';
import saga from './saga';
import reducer, { initialState } from './reducer';
import { makeSelectPage } from './selectors';
import { createStructuredSelector } from 'reselect';

/* eslint-disable react/prefer-stateless-function */
export class MultistepForm extends Component {
  render() {
    const { page } = this.props;
    return (
      <div>
        {page === 1 && <CoverageForm />}
        {page === 2 && <ShippingForm />}
      </div>
    );
  }
}

const pageSelector = (page, state) => state.get(page, initialState);
const mapStateToProps = createStructuredSelector({
  page: makeSelectPage(),
});
const withConnect = connect(mapStateToProps);
const withReducer = injectReducer({ key: 'page', reducer });
const withSaga = injectSaga({ key: 'page', saga });

MultistepForm.propTypes = {
  page: PropTypes.number,
  pageSelector: PropTypes.func,
};

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MultistepForm);
