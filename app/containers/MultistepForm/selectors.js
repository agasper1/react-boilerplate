import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectPage = state => state.get('page', initialState);

const makeSelectPage = () =>
  createSelector(selectPage, pageState => pageState.get('page', initialState));

export { selectPage, makeSelectPage };
