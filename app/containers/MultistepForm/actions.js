import { NEXT_PAGE, PREVIOUS_PAGE } from './constants';
/**
 * @name nextPage
 */
export function nextPage() {
  return {
    type: NEXT_PAGE,
  };
}

export function previousPage() {
  return {
    type: PREVIOUS_PAGE,
  };
}
