import { fromJS } from 'immutable';

import { NEXT_PAGE, PREVIOUS_PAGE } from './constants';

export const initialState = fromJS({
  page: 1,
});

function pageReducer(state = initialState, action) {
  console.log("pageReducer - state.get('page')", state.get('page'));
  switch (action.type) {
    case NEXT_PAGE:
      return state.set('page', state.get('page') + 1);
    case PREVIOUS_PAGE:
      return state.set('page', state.get('page') - 1);
    default:
      return state;
  }
}

const reducer = pageReducer;

export default reducer;
