import {
  SHIPPING_ACTION,
  SHIPPING_ACTION_SUCCESS,
  SHIPPING_ACTION_ERROR,
  LOG_FORM_FIELD_VALUES,
} from './constants';
export function logFormFieldValues() {
  return {
    type: LOG_FORM_FIELD_VALUES,
  };
}

export function shippingAction() {
  return {
    type: SHIPPING_ACTION,
  };
}

export function shippingActionError(error) {
  return {
    type: SHIPPING_ACTION_ERROR,
    error,
  };
}

export function shippingActionSuccess() {
  return {
    type: SHIPPING_ACTION_SUCCESS,
  };
}
