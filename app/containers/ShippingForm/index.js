import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import injectSelector from 'utils/injectSelector';
import { shippingAction } from './actions';
import saga from './saga';
import reducer from './reducer';
import { makeSelectFormValues as selector } from './selectors';

/* eslint-disable react/prefer-stateless-function */
export class ShippingForm extends Component {
  render() {
    const { handleSubmit, pristine, submitting, reset } = this.props;
    return (
      <form onSubmit={this.props.onSubmitForm}>
        <div>
          <label htmlFor="firstName">First Name</label>
          <Field
            name="firstName"
            component="input"
            type="text"
            placeholder="First name"
          />
        </div>
        <div>
          <label htmlFor="lastName">Last Name</label>
          <Field
            name="lastName"
            component="input"
            type="text"
            placeholder="Last name"
          />
        </div>
        <div>
          <label htmlFor="streetAddress1">Street Address 1</label>
          <Field
            name="streetAddress1"
            component="input"
            type="text"
            placeholder="Street Address 1"
          />
        </div>
        <div>
          <label htmlFor="streetAddress2">Street Address2</label>
          <Field
            name="streetAddress2"
            component="input"
            type="text"
            placeholder="Street Address 2"
          />
        </div>
        <div>
          <label htmlFor="state">State</label>
          <Field
            name="state"
            component="input"
            type="text"
            placeholder="State"
          />
        </div>
        <div>
          <label htmlFor="zipcode">Zip code</label>
          <Field
            name="zipcode"
            component="input"
            type="text"
            placeholder="Zip copde"
          />
        </div>
        <div>
          <button type="submit" disabled={pristine || submitting}>
            Submit
          </button>
          <button
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button>
        </div>
      </form>
    );
  }
}
ShippingForm.propTypes = {
  handleSubmit: PropTypes.func,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  reset: PropTypes.func,
  onSubmitForm: PropTypes.func,
  address: PropTypes.objectOf({
    streetAddress1: PropTypes.string,
    streetAddress2: PropTypes.string,
    zipcode: PropTypes.string,
    state: PropTypes.string,
    country: PropTypes.string,
  }),
};

const withReduxForm = reduxForm({
  form: 'ShippingDetailsForm',
  destroyOnUnmount: false,
});

const withReducer = injectReducer({ key: 'shipping', reducer });
const withSaga = injectSaga({ key: 'shipping', saga });
const withSelector = injectSelector({ key: 'shipping' }, selector);

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: event => {
      if (event !== undefined && event.preventDefault) event.preventDefault();
      dispatch(shippingAction());
    },
  };
}

// If I were really clever, I would just map over the form field names

const mapStateToProps = state => {
  const firstName = selector(state, 'firstName');
  const lastName = selector(state, 'lastName');
  const streetAddress1 = selector(state, 'streetAddress1');
  const streetAddress2 = selector(state, 'streetAddress2');
  const zipcode = selector(state, 'zipcode');
  const stateLocation = selector(state, 'state');

  return {
    firstName,
    lastName,
    streetAddress1,
    streetAddress2,
    zipcode,
    stateLocation,
  };
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReduxForm,
  withReducer,
  withSaga,
  withConnect,
)(ShippingForm);
