import { call, put, select, takeLatest } from 'redux-saga/effects';
import { SHIPPING_ACTION } from 'containers/ShippingForm/constants';
import {
  shippingAction,
  shippingActionError,
} from 'containers/ShippingForm/actions';

import request from 'utils/request';
import {
  // makeSelectAddress,
  makeSelectFormValues,
  // makeSelectState,
} from 'containers/ShippingForm/selectors';

export function* getShippingThing() {
  // const address = yield select(makeSelectAddress());
  // const fullName = yield select(makeSelectFullName());
  const formValues = yield select(makeSelectFormValues());
  console.log('formValues', formValues);
  // console.log('getShippingThing - address:', address);
  const addressValidator = 'https://someplace';
  try {
    // const validAddressResponse = yield call(request, requestURL);
    // yield put(shippingAction(address));
    // yield put(shippingAction());
  } catch (error) {
    yield put(shippingActionError(error));
  }
}

/**
 * Root saga manages watcher lifecycle
 */

export default function* shippingData() {
  yield takeLatest(SHIPPING_ACTION, getShippingThing);
}
