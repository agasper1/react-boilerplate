import { fromJS } from 'immutable';

// Set US to the default state
export const initialState = fromJS({
  address: {
    streetAddress1: '',
    streetAddress2: '',
    zipcode: '',
    state: '',
    country: 'US',
  },
  name: {
    firstName: '',
    lastName: '',
  },
});

const SOMETHING_WITH_SHIPPING = 'SOMETHING_WITH_SHIPPING';
function shippingReducer(state = initialState, action) {
  switch (action.type) {
    case SOMETHING_WITH_SHIPPING:
      return state;
    default:
      return state;
  }
}

const reducer = shippingReducer;

export default reducer;
