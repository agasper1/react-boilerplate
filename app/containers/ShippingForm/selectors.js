import { createSelector } from 'reselect';
import { getIn, fromJS } from 'immutable';
import { initialState } from './reducer';
const selectStreetAddress1 = state =>
  state.getIn(['shipping', 'address', 'streetAddress1'], initialState);
const selectStreetAddress2 = state =>
  state.getIn(['shipping', 'address', 'streetAddress2'], initialState);
const selectCity = state =>
  state.getIn(['shipping', 'address', 'city'], initialState);
const selectZipCode2 = state =>
  state.getIn(['shipping', 'address', 'zipcode'], initialState);
const selectState = state =>
  state.getIn(['shipping', 'address', 'state'], initialState);
const selectCountry = state =>
  state.getIn(['shipping', 'address', 'country'], initialState);

const selectFirstName = state =>
  state
    .get('shipping')
    .get('name')
    .get('firstName', initialState);
const selectLastName = state =>
  state
    .get('shipping')
    .get('name')
    .get('lastName', initialState);
const makeSelectFullName = () => {
  createSelector(
    selectFirstName,
    selectLastName,
    (firstName, lastName) =>
      `${firstName.get('firstName')}  ${lastName.get('lastName')}`,
  );
};

function getFormState(state, form) {
  return state.get('form').get(form);
}

const selectFormState = state =>
  fromJS({
    form: state.get('form', initialState),
  });
const makeSelectFormValues = () =>
  createSelector(selectFormState, state =>
    getFormState(state, 'ShippingDetailsForm'),
  );
export { makeSelectFullName, makeSelectFormValues };
