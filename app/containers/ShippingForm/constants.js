export const LOG_FORM_FIELD_VALUES = 'LOG_FORM_FIELD_VALUES';
export const SHIPPING_ACTION = 'SHIPPING_ACTION';
export const SHIPPING_ACTION_SUCCESS = 'SHIPPING_ACTION_SUCCESS';
export const SHIPPING_ACTION_ERROR = 'SHIPPING_ACTION_ERROR';
