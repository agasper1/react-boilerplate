import invariant from 'invariant';
import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import isString from 'lodash/isString';
import { createSelector } from 'reselect';
import checkStore from './checkStore';

// import checkStore from './checkStore';
// import checkProvider from './checkProvider'; // Maybe

export function injectSelectorFactory(store, isValid) {
  return function injectSelector(key, selector) {
    if (!isValid) checkStore(store);
    invariant(
      isString(key) && !isEmpty(key) && isFunction(selector),
      '(app/utils...) inejctSelector: Expected `selector` to be a selector function',
    );
    // Check `store.injectSelectors[key] === selector` for hot reloading when a key is the same but a selector is different
    // That being said, I have not set it up (yet?) to monitor the selectors

    if (
      Reflect.has(store.injectSelector, key) &&
      store.injectSelectors[key] === selector
    )
      return;
    store.injectSelectors[key] = selector; // eslint-disable-line no-param-reassign
    store.replaceSelector(createSelector(store.injectedSelectors));
  };
}

// Replace in case the selector has propagated
// The sagas seem to unmount themselves, but reducers do not
// And I figure a selector is closer to a reducer than a selector is closer to a saga
export default function getInjectors(store) {
  console.log('selectorInjectors.js - store', store);
  checkStore(store);
  return {
    injectSelector: injectSelectorFactory(store, true),
  };
}
