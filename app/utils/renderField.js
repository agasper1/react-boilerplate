import React from 'react';

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning, dirty },
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />

      {dirty &&
        ((error && <span className="formError">{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);
export default renderField;
