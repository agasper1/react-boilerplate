import React from 'react';
import PropTypes from 'prop-types';
import hoistNonReactStatics from 'hoist-non-react-statics';

import getInjectors from './selectorInjectors';

/**
 * Dynamically injects a selecotr
 *
 * @param {string} key - Name of the selector
 * @param {function} selector = A selector that will be injected
 *
 */

export default ({ key, selector }) => WrappedComponent => {
  class SelectorInjector extends React.Component {
    static WrappedComponent = WrappedComponent;
    static contextTypes = {
      store: PropTypes.object.isRequired,
    };
    static displayName = `withSelector(${WrappedComponent.displayName ||
      WrappedComponent.name ||
      'Component'})`;

    componentWillMount() {
      const { injectSelector } = this.injectors;

      injectSelector(key, selector);
    }

    injectors = getInjectors(this.context.store);

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return hoistNonReactStatics(SelectorInjector, WrappedComponent);
};
