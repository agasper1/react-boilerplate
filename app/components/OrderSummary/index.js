function OrderSummary(props) {
  return (
    <table>
      <thead>
        <th>Item</th>
        <th>Cost</th>
      </thead>
      <tbody>
        <tr>
          <td>Item 1</td>
          <td>Cost 1</td>
        </tr>
        <tr>
          <td>Item 2</td>
          <td>Cost 2</td>
        </tr>
      </tbody>
    </table>
  );
}
