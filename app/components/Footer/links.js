const footerLinks = [
  {
    title: 'Plan Terms & Conditions',
    href: 'https://www.mintmobile.com/plan-terms-and-conditions/',
    'data-ga': 'Footer|Menu Selection|Plan Terms &amp; Conditions',
  },
  {
    title: 'Privacy Policy',
    href: 'https://www.mintmobile.com/privacy-policy/',
    'data-ga': 'Footer|Menu Selection|Privacy Policy',
  },
  {
    title: 'Return Policy',
    href: 'https://www.mintmobile.com/return-policy/',
    'data-ga': 'Footer|Menu Selection|Return Policy',
  },
  {
    title: 'Acceptable Use Policy',
    href: 'https://www.mintmobile.com/acceptable-use-policy/',
    'data-ga': 'Footer|Menu Selection|Acceptable Use Policy',
  },
  {
    title: 'Site Terms of Use',
    href: 'https://www.mintmobile.com/site-terms-of-use/',
    'data-ga': 'Footer|Menu Selection|Site Terms of Use',
  },
  {
    title: '911 & E911 Disclosure',
    href: 'https://www.mintmobile.com/911-and-e911-disclosure/',
    'data-ga': 'Footer|Menu Selection|911 &amp; E911 Disclosure',
  },
  {
    title: 'Prepaid MTS Surcharge',
    href: 'https://www.mintmobile.com/ca-mts/',
    'data-ga': 'Footer|Menu Selection|Prepaid MTS Surcharge',
  },
];

export default footerLinks;
