import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from 'components/A';
import List from 'components/List';
import './Footer.scss';
// import Wrapper from './Wrapper';
import footerLinks from './links';
import messages from './messages';

function Footer() {
  return (
    <footer className="footer">
      <span>
        <FormattedMessage {...messages.copyrightMessage} />
      </span>
      <ul>
        {footerLinks.map(footerLink => (
          <li>
            <a
              href={`${footerLink.href}`}
              data-ga={`${footerLink['data-ga']}`}
            >{`${footerLink.title}`}</a>
          </li>
        ))}
      </ul>
      <span>
        <FormattedMessage {...messages.legalMessage} />
      </span>
    </footer>
  );
}

export default Footer;
