import React from 'react';
import PropTypes from 'prop-types';
import './Card.scss';

function Card(props) {
  const { title, body, image } = props;
  const card = (
    <div className="card">
      <div className="card-body">
        <div className="card-title">{title ? <h4>{title}</h4> : ''}</div>
        <div className="card-text">
          {/* {{image} ? <img src={require({image})} /> : ''} */}
          {image || ''}
          {body ? <p>{body}</p> : ''}
        </div>
      </div>
    </div>
  );

  return card;
}

Card.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  image: PropTypes.image,
};

export default Card;
