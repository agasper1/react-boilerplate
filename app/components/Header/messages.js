/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'boilerplate.components.Header.home',
    defaultMessage: 'Home',
  },
  features: {
    id: 'boilerplate.components.Header.features',
    defaultMessage: 'Features',
  },
  checkCoverage: {
    id: 'boilerplate.components.Header.checkCoverage',
    defaultMessage: 'Check coverage',
  },
  shipping: {
    id: 'boilerplate.components.Header.shipping',
    defaultMessage: 'Shipping',
  },
  payment: {
    id: 'boilerplate.components.Header.payment',
    defaultMessage: 'Payment',
  },
  products: {
    id: 'boilerplate.components.Header.products',
    defaultMessage: 'Products',
  },
});
