import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import MintMobileLogo from './mintmobile';
import './Header.scss';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  render() {
    return (
      <div className="Header container-fluid">
        <NavBar>
          <MintMobileLogo />
        </NavBar>
      </div>
    );
  }
}

export default Header;
