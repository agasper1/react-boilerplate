/**
 * Table
 */

import React from 'react';
import PropTypes from 'prop-types';

function Table(props) {
  const { headers, rows } = props;
  return (
    <table>
      <thead>{headers.map(header => <th>{header}</th>)}</thead>
      <tbody>
        {rows.map(row => <tr>{row.map(data => <td>{data}</td>)}</tr>)}
      </tbody>
    </table>
  );
}

Table.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string),
  rows: PropTypes.arrayOf(PropTypes.array),
};

export default Table;

// headers = ['Header 1', 'Header 2']
// rows = [[val1, val2], []]
// These are probably supposed to be like ...headers ...rows
