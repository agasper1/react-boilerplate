import React from 'react';

function SampleColumn() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">col</div>
        <div className="col">col</div>
        <div className="col">col</div>
        <div className="col">col</div>
      </div>
      <div className="row">
        <div className="col-8">col-8</div>
        <div className="col-4">col-4</div>
      </div>
    </div>
  );
}

export default SampleColumn;
