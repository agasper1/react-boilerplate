import React from 'react';
import PropTypes from 'prop-types';

import List from 'components/List';
import ListItem from 'components/ListItem';

import LoadingIndicator from 'components/LoadingIndicator';
import Card from 'components/Card';

/* eslint-disable react/prefer-stateless-function */
class ProductList extends React.Component {
  render() {
    let content;
    if (this.props.loading) {
      content = <List component={LoadingIndicator} />;
    }
    if (this.props.error) {
      const ErrorComponent = () => (
        <ListItem item="Are you foxing kidding me?" />
      );
      content = <List component={ErrorComponent} />;
    }

    if (this.props.products) {
      const { products } = this.props;
      content = products.map(product => (
        <Card title={product.title} body={product.body} />
      ));
    }
    return <ul>{content}</ul>;
  }
}

ProductList.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  products: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      body: PropTypes.string,
    }),
  ),
};

export default ProductList;
