# Where to take this component
1. SVG Icon to be on the left-hand side of the banner text like in the mobile design 
  - Use the mint mobile logo for now 
2. Make the SVG icon size configurable through props
  - <SvgIconComponent className={small | medium | large} />
  - Think like the expandable card where we had something like `className={props.cardBodyClassName}`
3. Make the banner component render all children. 
- You'll know you have "it", if you can do this: 
```
<Banner bannerClass="someClass" otherProps="stuff">
    <List>
    <ListItem>TRIAL INCLUDES: <span className="dontUseBTagsForBold>100MB</span></ListItem>
    <ListItem>Other text: <span className="dontUseBTagsForBold>other quantity</span></ListItem>
    </List>
</Banner>
```

Notes:
    Somewhere in the footer (s)css are the rules for making the dividers for the links go from  `link1 | link2 |...` to `link1 link2` based on screen size 