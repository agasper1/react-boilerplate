import React from 'react';
import PropTypes from 'prop-types';
import './Banner.scss';

function Banner(props) {
  const {
    bannerClass,
    svgIcon,
    svgIconClass,
    bannerColor,
    bannerTextClass,
    bannerText,
    bannerTextAsObject,
  } = props;

  return (
    <div style={{ backgroundColor: bannerColor }} className={bannerClass}>
      {svgIcon ? (
        <div className={svgIconClass}>
          {svgIcon}
          {bannerText ? <p align="right">{bannerText}</p> : ''}
        </div>
      ) : (
        ''
      )}
      <div className={bannerTextClass}>{bannerTextAsObject || ''}</div>
    </div>
  );
}

// svgIcon = PropTypes.Component ?
Banner.propTypes = {
  bannerClass: PropTypes.string,
  svgIcon: PropTypes.object,
  svgIconClass: PropTypes.string,
  bannerColor: PropTypes.string,
  bannerTextClass: PropTypes.string,
  bannerText: PropTypes.string,
  bannerTextAsObject: PropTypes.object,
};

export default Banner;
