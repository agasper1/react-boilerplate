import React from 'react';
import PropTypes from 'prop-types';
import './PlanCard.scss';
function PlanCard(props) {
  const { title, body } = props;
  const card = (
    <div className="card">
      <div className="card-body">
        <div className="card-title">
          {/* <em class="Plans-accent">Intro<br>offer</em> */}
          <div className="row no-gutters">
            <div className="col-6">
              <h4 className="Plans-price">
                <sup>$</sup>15
              </h4>
            </div>
            <div className="col-6">
              <p className="Plans-priceFeature">
                2GB <span>4G LTE</span>
              </p>
            </div>
          </div>
          <sub>Per month</sub>
        </div>
        <div className="card-text">
          <a
            href="#"
            className="btn btn-primary js-add-plan"
            data-ecomm="add|MINT-SMALL-03|MINT-SMALL-03|45|Mint Mobile|Plans|1"
            data-product-id="1096"
          >
            Buy now | Total $45
          </a>
          <small>Reg. 3 month price $23/mo</small>
        </div>
      </div>
      <div className="card-footer">
        <a
          href="/product/3-month-2gb-plan-sim-kit/"
          data-ecomm="click|MINT-SMALL-03|MINT-SMALL-03|45|Mint Mobile|Plans|1|Plans"
        >
          View details
        </a>
      </div>
    </div>
  );

  return card;
}

PlanCard.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
};

export default PlanCard;
